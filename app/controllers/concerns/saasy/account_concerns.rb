# frozen_string_literal: true

# Naturally many of the various resources which are scoped to accounts all need
# to be appropriately locked down in order to ensure that unauthorized users
# can not access them. This is where those concerns live
module Saasy
  module AccountConcerns
    extend ActiveSupport::Concern

    included do
      before_action :authorize_user!
      helper_method :current_account
    end

    def current_account
      @current_account ||= Saasy::Account.find_by!(subdomain: request.subdomain)
    end

    private

    def authorize_user!
      authenticate_user!
      return true if valid_account_member?
      flash[:notice] = 'You are not allowed to view this account.'
      redirect_to saasy.root_url(subdomain: nil)
    end

    def valid_account_member?
      current_account.users.exists?(current_user.id)
    end
  end
end
