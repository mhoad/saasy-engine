# frozen_string_literal: true

module Saasy
  module AdminConcerns
    extend ActiveSupport::Concern

    included do
      before_action :authorize_user!
    end

    private

    def authorize_user!
      authenticate_user!
      return true if application_admin?
      flash[:alert] = 'You do not have permission to access this page.'
      redirect_to(request.referrer || saasy.root_path)
    end

    def application_admin?
      current_user.has_role?(:admin)
    end
  end
end
