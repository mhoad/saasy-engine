# frozen_string_literal: true

require_dependency 'saasy/application_controller'

module Saasy
  module Accounts
    class DashboardController < ApplicationController
      include Saasy::AccountConcerns
      def index; end
    end
  end
end
