# frozen_string_literal: true

require_dependency 'saasy/application_controller'

module Saasy
  module Accounts
    class InvitationsController < ApplicationController
      include AccountConcerns
      skip_before_action :authorize_user!, only: %i[accept accepted]
      after_action :verify_authorized, except: %i[accept accepted]

      def new
        @invitation = current_account.invitations.new
        authorize! @invitation
      end

      def create
        @invitation = current_account.invitations.new(invitation_params)
        authorize! @invitation
        invite_user
      end

      def accept
        store_location_for(:user, request.fullpath)
        @invitation = Invitation.find_by!(token: params[:id])
      end

      def accepted
        @invitation = Invitation.find_by!(token: params[:id])
        signup_new_user unless user_signed_in?
        add_user_to_account
        redirect_to root_url(subdomain: current_account.subdomain)
      end

      private

      def authorize!(invitation)
        return if authorize invitation
        flash[:alert] = I18n.t('authorization.failure')
        redirect_to root_url(subdomain: current_account.subdomain)
      end

      def invitation_params
        params.require(:invitation).permit(:email)
      end

      def user_params
        params.require(:user).permit(:email, :password, :password_confirmation)
      end

      def signup_new_user
        result = Saasy::Users::CreateUser.call(user_params)
        if result.success?
          sign_in(result.user)
        else
          flash[:alert] = I18n.t('account.create.failure')
          render :accept
        end
      end

      def add_user_to_account
        result = Saasy::Memberships::AddUserToAccount.call(
          user: current_user,
          account: current_account
        )
        if result.success?
          flash[:notice] = I18n.t('invitation.accept.success', company_name: current_account.company_name)
        else
          flash[:alert] = I18n.t('invitation.accept.failure', company_name: current_account.company_name)
        end
      end

      def invite_user
        result = Saasy::Invitations::InviteUserToAccount.call(
          params: invitation_params,
          account: current_account
        )
        if result.success?
          flash[:notice] = I18n.t(result.message, email: result.invitation.email)
          redirect_to root_url
        else
          flash.now[:alert] = I18n.t(result.message, email: result.invitation.email)
          render :new
        end
      end
    end
  end
end
