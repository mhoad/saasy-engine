# frozen_string_literal: true

require_dependency 'saasy/application_controller'

module Saasy
  module Accounts
    class UsersController < ApplicationController
      include Saasy::AccountConcerns

      def index; end

      def destroy
        result = Saasy::Memberships::RemoveUserFromAccount.call(user: user_params, account: current_account)
        process_deletion_result(result)
        redirect_to users_path
      end

      private

      def user_params
        User.find(params[:id])
      end

      def process_deletion_result(result)
        if result.success?
          flash[:notice] = I18n.t(result.message, user: result.membership.user.email)
        else
          flash[:alert] = I18n.t(result.message, user: result.membership.user.email)
        end
      end
    end
  end
end
