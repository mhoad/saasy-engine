# frozen_string_literal: true

require_dependency 'saasy/application_controller'

module Saasy
  module Admin
    class AccountsController < ApplicationController
      include Saasy::AdminConcerns

      before_action :find_account, only: %i[edit update show destroy]

      def index
        @accounts = Saasy::Account.all
      end

      def new
        @account = Saasy::Account.new
      end

      def create
        @account = Saasy::Account.new
        result = Saasy::Accounts::CreateAccount.call(account_params)
        if result.success?
          flash[:notice] = I18n.t(result.message)
          redirect_to admin_account_path(result.account)
        else
          flash.now[:alert] = I18n.t(result.message)
          render :new
        end
      end

      def show; end

      def edit; end

      def update
        result = Saasy::Accounts::UpdateAccount.call(
          account: @account,
          new_attributes: account_params
        )
        if result.success?
          flash[:notice] = I18n.t(result.message)
          redirect_to admin_account_path(result.account)
        else
          flash.now[:alert] = I18n.t(result.message)
          render :edit
        end
      end

      def destroy
        result = Saasy::Accounts::DeleteAccount.call(account: @account)
        if result.success?
          flash[:notice] = I18n.t(result.message)
          redirect_to admin_accounts_path
        else
          flash[:alert] = I18n.t(result.message)
          redirect_to(request.referrer || admin_account_path(@account))
        end
      end

      private

      def find_account
        @account = Saasy::Account.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        flash[:alert] = I18n.t('admin.errors.account_not_found')
        redirect_to(request.referrer || admin_account_path)
      end

      def account_params
        params.require(:account).permit(:company_name, :subdomain)
      end
    end
  end
end
