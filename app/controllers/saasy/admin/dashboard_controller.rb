# frozen_string_literal: true

require_dependency 'saasy/application_controller'

module Saasy
  module Admin
    class DashboardController < ApplicationController
      include Saasy::AdminConcerns
      def index; end
    end
  end
end
