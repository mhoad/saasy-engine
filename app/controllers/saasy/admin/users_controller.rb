# frozen_string_literal: true

require_dependency 'saasy/application_controller'

module Saasy
  module Admin
    class UsersController < ApplicationController
      include Saasy::AdminConcerns

      before_action :find_user, only: %i[show destroy]

      def index
        @users = Saasy::User.all
      end

      def show; end

      def destroy
        result = Saasy::Users::DeleteUser.call(user: @user)
        if result.success?
          flash[:notice] = I18n.t(result.message)
          redirect_to admin_users_path
        else
          flash[:alert] = I18n.t(result.message)
          redirect_to(request.referrer || admin_user_path(@user))
        end
      end

      private

      def find_user
        @user = Saasy::User.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        flash[:alert] = I18n.t('admin.errors.user_not_found')
        redirect_to(request.referrer || admin_users_path)
      end
    end
  end
end
