# frozen_string_literal: true

module Saasy
  class ApplicationController < ActionController::Base
    include Pundit
    protect_from_forgery with: :exception

    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

    private

    def user_not_authorized
      flash[:alert] = I18n.t('authorization.failure')
      redirect_to(request.referrer || saasy.root_path)
    end
  end
end
