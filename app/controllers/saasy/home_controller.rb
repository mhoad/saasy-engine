# frozen_string_literal: true

require_dependency 'saasy/application_controller'

module Saasy
  class HomeController < ApplicationController
    def index; end
  end
end
