# frozen_string_literal: true

require_dependency 'saasy/application_controller'

module Saasy
  class SignupsController < ApplicationController
    def new
      @signup = Saasy::SignupForm.new
    end

    def create
      @signup = Saasy::SignupForm.new(signup_params)
      result = @signup.save
      if result.success?
        handle_successful_signup(result)
      else
        flash.now[:alert] = I18n.t('account.create.failure')
        render action: :new
      end
    end

    private

    def signup_params
      params.require(:signup_form).permit(:company_name, :subdomain, :email, :password)
    end

    def handle_successful_signup(result)
      sign_in result.user
      flash[:notice] = I18n.t('account.create.success')
      redirect_to saasy.root_url(subdomain: result.account.subdomain)
    end
  end
end
