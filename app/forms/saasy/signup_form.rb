# frozen_string_literal: true

module Saasy
  class SignupForm
    include ActiveModel::Model

    attr_accessor :company_name, :subdomain, :email, :password
    validates :company_name, :subdomain, :email, :password, presence: true

    def save
      fail! unless valid?
      result = Saasy::ProcessSignup.call(
        company_name: company_name,
        subdomain: subdomain,
        email: email,
        password: password
      )
      result.user.add_role :admin, result.account if result.success?

      result
    end

    private

    def fail!
      OpenStruct.new(success?: false, errors: errors.messages,
                     message: 'account.create.failure')
    end
  end
end
