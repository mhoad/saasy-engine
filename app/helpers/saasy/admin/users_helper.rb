# frozen_string_literal: true

module Saasy
  module Admin
    module UsersHelper
      def able_to_remove_user?(user)
        # Don't allow the final user to remove themselves
        return false if current_account.users.count < 2
        # Those who can remove users are able to do so
        return true if current_user.has_role?(:admin, current_account)
        return true if current_user.has_role?(:admin)
        return true if current_user == user
        # Otherwise the default answer is no
        false
      end
    end
  end
end
