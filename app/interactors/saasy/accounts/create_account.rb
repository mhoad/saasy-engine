# frozen_string_literal: true

module Saasy
  module Accounts
    class CreateAccount
      include Interactor

      def call
        record = Saasy::Account.create(
          company_name: context.company_name,
          subdomain: context.subdomain
        )
        handle_failure(record) unless record.persisted?
        context.account = record
        context.message = 'account.create.success'
      end

      def rollback
        context.account.destroy
      end

      private

      def handle_failure(record)
        context.fail!(
          message: 'account.create.failure',
          errors: record.errors.messages
        )
      end
    end
  end
end
