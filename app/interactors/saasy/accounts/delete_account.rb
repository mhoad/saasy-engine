# frozen_string_literal: true

module Saasy
  module Accounts
    class DeleteAccount
      include Interactor

      def call
        context.account.destroy
        handle_failure if context.account.deleted_at.nil?
        context.message = 'account.delete.success'
      end

      def rollback
        context.account.restore
      end

      private

      def handle_failure
        context.fail!(
          message: 'account.delete.failure',
          errors: context.account.errors.messages
        )
      end
    end
  end
end
