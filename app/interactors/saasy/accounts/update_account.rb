# frozen_string_literal: true

# Receives: account - A Saasy::Account record to update.
#           new_attributes - A hash of attributes to be saved to the account.
# Updates:  account
# Provides: old_attributes - A hash of only those account attributes that
#           changed and their original values.
module Saasy
  module Accounts
    class UpdateAccount
      include Interactor

      def call
        context.account.attributes = context.new_attributes
        context.old_attributes = context.account.changed_attributes
        handle_failure unless context.account.save
        context.message = 'account.update.success'
      end

      def rollback
        context.account.update(context.old_attributes)
      end

      private

      def handle_failure
        context.fail!(
          message: 'account.update.failure',
          errors: context.account.errors.messages
        )
      end
    end
  end
end
