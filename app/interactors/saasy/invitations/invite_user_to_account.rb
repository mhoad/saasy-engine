# frozen_string_literal: true

module Saasy
  module Invitations
    class InviteUserToAccount
      include Interactor

      def call
        context.invitation = Saasy::Invitation.create(
          email: context.params[:email],
          account: context.account
        )
        if context.invitation.persisted?
          InvitationMailer.invite(context.invitation).deliver_now
          context.message = 'invitation.create.success'
        else
          context.fail!(
            message: 'invitation.create.failure',
            errors: context.invitation.errors
          )
        end
      end
    end
  end
end
