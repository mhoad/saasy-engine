# frozen_string_literal: true

module Saasy
  module Memberships
    class AddUserToAccount
      include Interactor

      def call
        record = Saasy::Membership.create(
          user_id: context.user.try(:id),
          account_id: context.account.try(:id)
        )

        handle_failure(record) unless record.persisted?

        context.membership = record
        context.message = 'membership.create.success'
      end

      def rollback
        context.membership.destroy
      end

      private

      def handle_failure(record)
        context.fail!(
          message: 'membership.create.failure',
          errors: record.errors.messages
        )
      end
    end
  end
end
