# frozen_string_literal: true

module Saasy
  module Memberships
    class RemoveUserFromAccount
      include Interactor

      def call
        check_parameters
        context.membership = Saasy::Membership.find_by(
          user: context.user,
          account: context.account
        )
        record = context.membership.destroy
        handle_failure(record) unless record.destroyed?

        context.membership = record
        context.message = 'membership.destroy.success'
      end

      def rollback
        return if context.membership.nil?
        Saasy::Membership.create(context.membership.attributes)
      end

      private

      def handle_failure(record)
        context.fail!(
          message: 'membership.destroy.failure',
          errors: record.errors.messages
        )
      end

      def check_parameters
        return if !context.account.nil? && !context.user.nil?
        context.fail!(message: 'membership.destroy.failure')
      end
    end
  end
end
