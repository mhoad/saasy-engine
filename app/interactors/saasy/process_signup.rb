# frozen_string_literal: true

module Saasy
  class ProcessSignup
    include Interactor::Organizer

    organize(
      Saasy::Accounts::CreateAccount,
      Saasy::Users::CreateUser,
      Saasy::Memberships::AddUserToAccount
    )
  end
end
