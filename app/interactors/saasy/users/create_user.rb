# frozen_string_literal: true

module Saasy
  module Users
    class CreateUser
      include Interactor

      def call
        record = Saasy::User.create(email: context.email, password: context.password)
        handle_failure(record) unless record.persisted?
        context.user = record
        context.message = 'user.create.success'
      end

      def rollback
        context.user.destroy
      end

      private

      def handle_failure(record)
        context.fail!(
          message: 'user.create.failure',
          errors: record.errors.messages
        )
      end
    end
  end
end
