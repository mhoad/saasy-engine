# frozen_string_literal: true

module Saasy
  module Users
    class DeleteUser
      include Interactor

      def call
        context.user.destroy
        handle_failure if context.user.deleted_at.nil?
        context.message = 'user.delete.success'
      end

      def rollback
        context.user.restore
      end

      private

      def handle_failure
        context.fail!(
          message: 'user.delete.failure',
          errors: context.user.errors.messages
        )
      end
    end
  end
end
