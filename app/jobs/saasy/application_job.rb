# frozen_string_literal: true

module Saasy
  class ApplicationJob < ActiveJob::Base
  end
end
