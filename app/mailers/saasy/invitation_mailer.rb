# frozen_string_literal: true

module Saasy
  class InvitationMailer < ApplicationMailer
    def invite(invitation)
      @invitation = invitation
      mail(
        to: invitation.email,
        subject: "Invitation to join #{invitation.account.company_name} on #{Saasy.application_name}"
      )
    end
  end
end
