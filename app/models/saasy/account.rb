# frozen_string_literal: true

# == Schema Information
#
# Table name: saasy_accounts
#
#  id           :integer          not null, primary key
#  company_name :string(50)       not null
#  subdomain    :string(50)       not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  deleted_at   :datetime
#

module Saasy
  class Account < ApplicationRecord
    acts_as_paranoid

    before_validation :normalise_data

    has_many :memberships
    has_many :users, through: :memberships
    has_many :invitations
    has_many :things, class_name: Saasy.thing_class

    validates :company_name, presence: true, length: { minimum: 2, maximum: 50 }
    validates :subdomain, presence: true, length: { minimum: 2, maximum: 50 }
    validates :subdomain, uniqueness: true
    validates_format_of :subdomain, with: /\A[a-zA-Z]+\z/, message: 'only letters are valid subdomains'
    validates :subdomain, exclusion: {
      in: %w[www admin about beta billing blog cdn chat cpanel download email
             search smtp pop mail mobile my news payment porn purchase affiliate],
      message: '%<value>% is not a valid subdomain.'
    }

    # Allow user roles to be set at the account level using the Rolify gem
    resourcify

    private

    def normalise_data
      # http://www.rubydoc.info/gems/rubocop/0.43.0/RuboCop/Cop/Style/SafeNavigation
      subdomain&.downcase! # same as subdomain.downcase! if !subdomain.nil?
      subdomain&.delete!(' ') # don't allow any spaces in subdomains
    end
  end
end
