# frozen_string_literal: true

module Saasy
  class ApplicationRecord < ActiveRecord::Base
    self.abstract_class = true
  end
end
