# frozen_string_literal: true

# == Schema Information

#
# Table name: saasy_invitations
#
#  id         :integer          not null, primary key
#  email      :string           not null
#  token      :string           not null
#  account_id :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module Saasy
  class Invitation < ApplicationRecord
    belongs_to :account, class_name: 'Saasy::Account', foreign_key: :account_id
    before_create :generate_token
    validates :email, presence: true
    validates :account_id, presence: true

    # We want to use the invitation token instead of 'id' as the parameter
    def to_param
      token
    end

    private

    def generate_token
      self.token = SecureRandom.hex(16)
    end
  end
end
