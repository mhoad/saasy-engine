# frozen_string_literal: true

# == Schema Information
#
# Table name: saasy_memberships
#
#  id         :integer          not null, primary key
#  account_id :integer          not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module Saasy
  class Membership < ApplicationRecord
    belongs_to :account, class_name: 'Saasy::Account', foreign_key: :account_id
    belongs_to :user, class_name: 'Saasy::User', foreign_key: :user_id

    validates :account_id, presence: true
    validates :user_id, presence: true
  end
end
