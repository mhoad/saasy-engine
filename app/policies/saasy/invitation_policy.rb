# frozen_string_literal: true

module Saasy
  class InvitationPolicy < Saasy::ApplicationPolicy
    def new?
      create?
    end

    def create?
      admin?
    end

    class Scope < Scope
      def resolve; end
    end

    private

    def admin?
      user.has_role?(:admin, record.account) || user.has_role?(:admin)
    end
  end
end
