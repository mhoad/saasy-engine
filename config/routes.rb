# frozen_string_literal: true

require 'saasy/constraints/subdomain_required'

Saasy::Engine.routes.draw do
  # Check for subdomains before matching any other routes
  constraints(Saasy::Constraints::SubdomainRequired) do
    scope module: 'accounts' do
      root to: 'dashboard#index', as: :account_root
      resources :users, only: %i[index destroy]
      resources :invitations, only: %i[new create] do
        member do
          get :accept
          patch :accepted
        end
      end
    end
  end

  devise_for :users, class_name: 'Saasy::User', module: :devise

  root to: 'home#index'
  get 'signup' => 'signups#new',     as: :new_signup
  post 'signup' => 'signups#create', as: :signups

  namespace :admin do
    root to: 'dashboard#index'
    resources :users, except: %i[edit update]
    resources :accounts
  end
end
