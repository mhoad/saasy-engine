# frozen_string_literal: true

class CreateSaasyAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :saasy_accounts do |t|
      t.string :company_name, limit: 50, null: false, comment: 'Name of the company who runs the account'
      t.string :subdomain, limit: 50, null: false, comment: 'Subdomain associated with the account'

      t.timestamps
    end

    add_index :saasy_accounts, :subdomain, unique: true
  end
end
