# frozen_string_literal: true

class CreateSaasyMemberships < ActiveRecord::Migration[5.2]
  def change
    create_table :saasy_memberships do |t|
      t.bigint :account_id, foreign_key: true, null: false, comment: 'What account is the user a member of'
      t.bigint :user_id, foreign_key: true, null: false, comment: 'Which user is a member of the account'

      t.timestamps
    end
  end
end
