# frozen_string_literal: true

class RolifyCreateSaasyRoles < ActiveRecord::Migration[5.2]
  def change
    create_table(:saasy_roles) do |t|
      t.string :name
      t.references :resource, polymorphic: true

      t.timestamps
    end

    create_table(:saasy_users_saasy_roles, id: false) do |t|
      t.references :user
      t.references :role
    end

    add_index(:saasy_roles, %i[name resource_type resource_id])
    add_index(:saasy_users_saasy_roles, %i[user_id role_id])
  end
end
