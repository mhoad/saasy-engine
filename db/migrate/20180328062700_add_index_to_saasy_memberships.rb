# frozen_string_literal: true

class AddIndexToSaasyMemberships < ActiveRecord::Migration[5.2]
  def change
    add_index :saasy_memberships, :account_id
    add_index :saasy_memberships, :user_id
    add_index(:saasy_memberships, %i[account_id user_id], unique: true)
  end
end
