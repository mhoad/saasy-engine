# frozen_string_literal: true

class AddDeletedAtToSaasyAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :saasy_accounts, :deleted_at, :datetime
    add_index :saasy_accounts, :deleted_at
  end
end
