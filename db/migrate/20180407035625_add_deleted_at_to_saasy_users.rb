# frozen_string_literal: true

class AddDeletedAtToSaasyUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :saasy_users, :deleted_at, :datetime
    add_index :saasy_users, :deleted_at
  end
end
