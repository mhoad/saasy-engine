# frozen_string_literal: true

class CreateSaasyInvitations < ActiveRecord::Migration[5.2]
  def change
    create_table :saasy_invitations do |t|
      t.string :email, null: false
      t.string :token, null: false
      t.bigint :account_id, foreign_key: true, null: false, comment: 'What account is the invitation for?'

      t.timestamps
    end

    add_index :saasy_invitations, :token
  end
end
