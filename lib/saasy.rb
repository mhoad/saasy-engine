# frozen_string_literal: true

require 'saasy/engine'
require 'rolify'
require 'devise'
require 'paranoia'
require 'pundit'
require 'interactor'

module Saasy
  mattr_accessor :application_name
  mattr_accessor :thing_class
end
