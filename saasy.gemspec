# frozen_string_literal: true

$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'saasy/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'saasy'
  s.version     = Saasy::VERSION
  s.authors     = ['Mark Hoad']
  s.email       = ['markdhoad@gmail.com']
  s.homepage    = 'https://gitlab.com/mhoad/saasy'
  s.summary     = 'Rails 5 Multi-Tenancy Engine'
  s.description = 'All the core functionality required to run a SaaS business'
  s.license     = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_dependency 'devise'
  s.add_dependency 'interactor-rails', '~> 2.0'
  s.add_dependency 'paranoia', '~> 2.4'
  s.add_dependency 'pundit', '~> 1.1'
  s.add_dependency 'rails', '~> 5.2.0.rc2'
  s.add_dependency 'rolify', '~> 5.2'

  s.add_development_dependency 'annotate'
  s.add_development_dependency 'capybara', '~> 2.17'
  s.add_development_dependency 'email_spec', '~> 2.2'
  s.add_development_dependency 'pg'
  s.add_development_dependency 'rspec-rails', '~> 3.7'
  s.add_development_dependency 'rubocop'
end
