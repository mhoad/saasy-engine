# frozen_string_literal: true

require 'rails_helper'

module Saasy
  module Admin
    RSpec.describe AccountsController, type: :controller do
      describe 'includes the correct concerns' do
        it { expect(controller.class.ancestors.include?(Saasy::AdminConcerns)).to eq(true) }
      end
    end
  end
end
