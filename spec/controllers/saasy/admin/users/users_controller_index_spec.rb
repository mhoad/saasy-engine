# frozen_string_literal: true

require 'rails_helper'

module Saasy
  module Admin
    RSpec.describe UsersController, type: :controller do
      routes { Saasy::Engine.routes }

      let!(:admin_user) { FactoryBot.create(:saasy_user) }

      context 'as an authenticated admin user' do
        before do
          admin_user.add_role :admin
          sign_in admin_user
        end

        describe 'GET #index' do
          before do
            get :index
          end

          it 'returns the correct HTTP status' do
            expect(response).to have_http_status(200)
          end

          it 'returns the correct header type' do
            expect(response.headers['Content-Type']).to eq 'text/html; charset=utf-8'
          end

          it 'correctly assigns the instance variable' do
            expect(assigns(:users)).to eq(Saasy::User.all)
          end

          it 'renders the correct template' do
            expect(response).to render_template(:index)
          end
        end
      end
    end
  end
end
