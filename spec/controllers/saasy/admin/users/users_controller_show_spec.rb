# frozen_string_literal: true

require 'rails_helper'

module Saasy
  module Admin
    RSpec.describe UsersController, type: :controller do
      routes { Saasy::Engine.routes }

      let!(:admin_user) { FactoryBot.create(:saasy_user) }

      context 'as an authenticated admin user' do
        before do
          admin_user.add_role :admin
          sign_in admin_user
        end

        describe 'GET #show' do
          context 'with a valid user' do
            before do
              params = { id: admin_user.id }
              get :show, params: params
            end

            it 'returns the correct HTTP status' do
              expect(response).to have_http_status(200)
            end

            it 'returns the correct header type' do
              expect(response.headers['Content-Type']).to eq 'text/html; charset=utf-8'
            end

            it 'correctly assigns the instance variable' do
              expect(assigns(:user)).to eq(Saasy::User.find(admin_user.id))
            end

            it 'renders the correct template' do
              expect(response).to render_template(:show)
            end
          end

          context 'with a non-existant user' do
            before do
              params = { id: -1 }
              get :show, params: params
            end

            it 'shows a flash message' do
              expect(flash[:alert]).to eq I18n.t('admin.errors.user_not_found')
            end

            it 'redirects to the admin users index' do
              expect(response).to redirect_to admin_users_path
            end
          end
        end
      end
    end
  end
end
