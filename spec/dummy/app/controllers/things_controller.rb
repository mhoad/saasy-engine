# frozen_string_literal: true

class ThingsController < ApplicationController
  include Saasy::AccountConcerns

  def index
    @things = current_account.things
  end
end
