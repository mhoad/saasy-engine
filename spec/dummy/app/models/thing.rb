# frozen_string_literal: true

class Thing < ApplicationRecord
  belongs_to :account, class_name: 'Saasy::Account'
end
