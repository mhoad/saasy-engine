# frozen_string_literal: true

# Set the name of your application here
Saasy.application_name = 'My SaaS app'

# Set the name of the main class which will live under accounts
Saasy.thing_class = 'Thing'

# In order to correctly handle flash messages accross subdomains
# you will need to have the following settings.
domains = {
  development: 'lvh.me',
  test: 'lvh.me',
  production: 'example.com'
}

options = {
  key: "_#{Saasy.application_name.parameterize.underscore}",
  domain: domains[Rails.env.to_sym]
}

Rails.application.config.session_store :cookie_store, options
