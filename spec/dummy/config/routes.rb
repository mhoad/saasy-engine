# frozen_string_literal: true

Rails.application.routes.draw do
  get '/things' => 'things#index', as: :things
  mount Saasy::Engine => '/'
end
