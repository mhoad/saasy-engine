# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_05_03_035800) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "saasy_accounts", force: :cascade do |t|
    t.string "company_name", limit: 50, null: false, comment: "Name of the company who runs the account"
    t.string "subdomain", limit: 50, null: false, comment: "Subdomain associated with the account"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_saasy_accounts_on_deleted_at"
    t.index ["subdomain"], name: "index_saasy_accounts_on_subdomain", unique: true
  end

  create_table "saasy_invitations", force: :cascade do |t|
    t.string "email", null: false
    t.string "token", null: false
    t.bigint "account_id", null: false, comment: "What account is the invitation for?"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["token"], name: "index_saasy_invitations_on_token"
  end

  create_table "saasy_memberships", force: :cascade do |t|
    t.bigint "account_id", null: false, comment: "What account is the user a member of"
    t.bigint "user_id", null: false, comment: "Which user is a member of the account"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id", "user_id"], name: "index_saasy_memberships_on_account_id_and_user_id", unique: true
    t.index ["account_id"], name: "index_saasy_memberships_on_account_id"
    t.index ["user_id"], name: "index_saasy_memberships_on_user_id"
  end

  create_table "saasy_roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_saasy_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_saasy_roles_on_resource_type_and_resource_id"
  end

  create_table "saasy_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_saasy_users_on_deleted_at"
    t.index ["email"], name: "index_saasy_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_saasy_users_on_reset_password_token", unique: true
  end

  create_table "saasy_users_saasy_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_saasy_users_saasy_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_saasy_users_saasy_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_saasy_users_saasy_roles_on_user_id"
  end

  create_table "things", force: :cascade do |t|
    t.string "name"
    t.integer "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
