# frozen_string_literal: true

FactoryBot.define do
  factory :thing, class: 'Thing' do
    name { Faker::Company.name }
    association :account, factory: :saasy_account

    trait :invalid do
      account nil
    end
  end
end
