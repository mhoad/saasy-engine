# frozen_string_literal: true

# == Schema Information
#
# Table name: saasy_accounts
#
#  id           :integer          not null, primary key
#  company_name :string(50)       not null
#  subdomain    :string(50)       not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  deleted_at   :datetime
#

FactoryBot.define do
  factory :saasy_account, class: 'Saasy::Account' do
    company_name { Faker::Company.name }
    subdomain { Faker::Internet.unique.domain_word }

    trait :invalid do
      company_name nil
    end
  end
end
