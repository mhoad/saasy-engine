# frozen_string_literal: true

# == Schema Information

#
# Table name: saasy_invitations
#
#  id         :integer          not null, primary key
#  email      :string           not null
#  token      :string           not null
#  account_id :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :saasy_invitation, class: 'Saasy::Invitation' do
    email { Faker::Internet.unique.email }
    association :account, factory: :saasy_account

    trait :invalid do
      email nil
    end
  end
end
