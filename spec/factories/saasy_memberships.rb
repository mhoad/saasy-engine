# frozen_string_literal: true

# == Schema Information
#
# Table name: saasy_memberships
#
#  id         :integer          not null, primary key
#  account_id :integer          not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :saasy_membership, class: 'Saasy::Membership' do
    association :account, factory: :saasy_account
    association :user, factory: :saasy_user

    trait :invalid do
      account nil
    end
  end
end
