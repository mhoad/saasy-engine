# frozen_string_literal: true

# == Schema Information
#
# Table name: saasy_roles
#
#  id            :integer          not null, primary key
#  name          :string
#  resource_type :string
#  resource_id   :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryBot.define do
  factory :saasy_role, class: 'Saasy::Role' do
  end
end
