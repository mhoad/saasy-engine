# frozen_string_literal: true

# == Schema Information
#
# Table name: saasy_users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  deleted_at             :datetime
#

FactoryBot.define do
  factory :saasy_user, class: 'Saasy::User' do
    email { Faker::Internet.unique.email }
    password { Faker::Internet.password }

    trait :invalid do
      email nil
    end
  end
end
