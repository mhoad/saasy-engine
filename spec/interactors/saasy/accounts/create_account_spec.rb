# frozen_string_literal: true

require 'rails_helper'

module Saasy
  RSpec.describe Saasy::Accounts::CreateAccount do
    describe '.call' do
      context 'when given valid attributes' do
        let(:account_params) { attributes_for(:saasy_account) }
        subject(:context) { Saasy::Accounts::CreateAccount.call(account_params) }

        it 'succeeds' do
          expect(context).to be_a_success
        end

        it 'provides the account' do
          expect(context.account).to be_valid
        end

        it 'provides a message' do
          expect(context.message).to eq('account.create.success')
        end
      end

      context 'when given invalid attributes' do
        let(:account_params) { attributes_for(:saasy_account, :invalid) }
        subject(:context) { Saasy::Accounts::CreateAccount.call(account_params) }

        it 'fails' do
          expect(context).to_not be_a_success
        end

        it 'does not provide an account' do
          expect(context.account).to eq nil
        end

        it 'provides a message' do
          expect(context.message).to eq('account.create.failure')
        end

        it 'provides the errors that caused it to fail' do
          expect(context.errors).to_not be_blank
        end
      end
    end
  end
end
