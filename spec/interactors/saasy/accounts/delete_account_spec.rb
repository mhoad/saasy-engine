# frozen_string_literal: true

require 'rails_helper'

module Saasy
  RSpec.describe Saasy::Accounts::UpdateAccount do
    describe '.call' do
      let!(:account) { FactoryBot.create(:saasy_account) }

      subject(:context) { Saasy::Accounts::DeleteAccount.call(account: account) }

      it 'succeeds' do
        expect(context).to be_a_success
      end

      it 'updates the deleted at column' do
        expect(context.account.deleted_at).to_not be_nil
      end

      it 'provides a message' do
        expect(context.message).to eq('account.delete.success')
      end
    end
  end
end
