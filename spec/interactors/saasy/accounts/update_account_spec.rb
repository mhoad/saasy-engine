# frozen_string_literal: true

require 'rails_helper'

module Saasy
  RSpec.describe Saasy::Accounts::UpdateAccount do
    describe '.call' do
      let!(:account) { FactoryBot.create(:saasy_account) }

      context 'when given valid attributes' do
        let(:params) do
          {
            subdomain: 'eggboss',
            company_name: 'Egg Co'
          }
        end

        subject(:context) { Saasy::Accounts::UpdateAccount.call(account: account, new_attributes: params) }

        it 'succeeds' do
          expect(context).to be_a_success
        end

        it 'provides the account' do
          expect(context.account).to be_valid
        end

        it 'provides a message' do
          expect(context.message).to eq('account.update.success')
        end

        it 'updates the email address' do
          expect(context.account.subdomain).to eq 'eggboss'
        end

        it 'updates the company name' do
          expect(context.account.company_name).to eq 'Egg Co'
        end
      end

      context 'with invalid attributes' do
        let(:params) do
          {
            subdomain: 'admin',
            company_name: 'Egg Co'
          }
        end

        subject(:context) { Saasy::Accounts::UpdateAccount.call(account: account, new_attributes: params) }

        it 'succeeds' do
          expect(context).to_not be_a_success
        end

        it 'provides a message' do
          expect(context.message).to eq('account.update.failure')
        end

        it 'informs us about the specific errors' do
          expect(context.errors.to_s).to include 'not a valid subdomain.'
        end
      end
    end
  end
end
