# frozen_string_literal: true

require 'rails_helper'

module Saasy
  RSpec.describe Saasy::Invitations::InviteUserToAccount do
    describe '.call' do
      let(:account) { FactoryBot.create(:saasy_account) }

      context 'when given valid attributes' do
        let(:invite_params) { attributes_for(:saasy_invitation) }
        subject(:context) { Saasy::Invitations::InviteUserToAccount.call(params: invite_params, account: account) }

        it 'succeeds' do
          expect(context).to be_a_success
        end

        it 'provides a valid invitation' do
          expect(context.invitation).to be_valid
        end

        it 'provides a message' do
          expect(context.message).to eq('invitation.create.success')
        end
      end

      context 'when given invalid attributes' do
        let(:invite_params) { attributes_for(:saasy_invitation, :invalid) }
        subject(:context) { Saasy::Invitations::InviteUserToAccount.call(params: invite_params, account: account) }

        it 'fails' do
          expect(context).to_not be_a_success
        end

        it 'does an invalid account' do
          expect(context.invitation).to_not be_valid
        end

        it 'provides a message' do
          expect(context.message).to eq('invitation.create.failure')
        end

        it 'provides the errors that caused it to fail' do
          expect(context.errors).to_not be_blank
        end
      end
    end
  end
end
