# frozen_string_literal: true

require 'rails_helper'

module Saasy
  RSpec.describe Saasy::Memberships::AddUserToAccount do
    describe '.call' do
      context 'when given valid attributes' do
        let(:user) { create(:saasy_user) }
        let(:account) { create(:saasy_account) }
        before do
          @membership_params = { user: user, account: account }
        end

        subject(:context) { Saasy::Memberships::AddUserToAccount.call(@membership_params) }

        it 'succeeds' do
          expect(context).to be_a_success
        end

        it 'provides the account' do
          expect(context.membership).to be_valid
        end

        it 'provides a message' do
          expect(context.message).to eq('membership.create.success')
        end
      end

      context 'when given invalid attributes' do
        let(:membership_params) { attributes_for(:saasy_membership, :invalid) }
        subject(:context) { Saasy::Memberships::AddUserToAccount.call(membership_params) }

        it 'fails' do
          expect(context).to_not be_a_success
        end

        it 'does not provide an account' do
          expect(context.membership).to eq nil
        end

        it 'provides a message' do
          expect(context.message).to eq('membership.create.failure')
        end

        it 'provides the errors that caused it to fail' do
          expect(context.errors).to_not be_blank
        end
      end
    end
  end
end
