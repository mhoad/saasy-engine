# frozen_string_literal: true

require 'rails_helper'

module Saasy
  RSpec.describe Saasy::Memberships::RemoveUserFromAccount do
    describe '.call' do
      let(:membership) { create(:saasy_membership) }

      context 'when given valid attributes' do
        before do
          @interactor_params = { user: membership.user, account: membership.account }
        end

        subject(:context) { Saasy::Memberships::RemoveUserFromAccount.call(@interactor_params) }

        it 'succeeds' do
          expect(context).to be_a_success
        end

        it 'provides a message' do
          expect(context.message).to eq('membership.destroy.success')
        end

        it 'destroys the membership record' do
          expect(context.membership.destroyed?).to be true
        end
      end

      context 'when given invalid attributes' do
        before do
          @interactor_params = { user: membership.user, account: nil }
        end

        subject(:context) { Saasy::Memberships::RemoveUserFromAccount.call(@interactor_params) }

        it 'fails' do
          expect(context).to be_a_failure
        end

        it 'provides a message' do
          expect(context.message).to eq('membership.destroy.failure')
        end

        it 'does not destroy the membership' do
          expect(membership.destroyed?).to be false
        end
      end
    end
  end
end
