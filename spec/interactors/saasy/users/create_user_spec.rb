# frozen_string_literal: true

require 'rails_helper'

module Saasy
  RSpec.describe Saasy::Users::CreateUser do
    describe '.call' do
      context 'when given valid attributes' do
        let(:user_params) { attributes_for(:saasy_user) }
        subject(:context) { Saasy::Users::CreateUser.call(user_params) }

        it 'succeeds' do
          expect(context).to be_a_success
        end

        it 'provides the account' do
          expect(context.user).to be_valid
        end

        it 'provides a message' do
          expect(context.message).to eq('user.create.success')
        end
      end

      context 'when given invalid attributes' do
        let(:user_params) { attributes_for(:saasy_user, :invalid) }
        subject(:context) { Saasy::Users::CreateUser.call(user_params) }

        it 'fails' do
          expect(context).to_not be_a_success
        end

        it 'does not provide an account' do
          expect(context.user).to eq nil
        end

        it 'provides a message' do
          expect(context.message).to eq('user.create.failure')
        end

        it 'provides the errors that caused it to fail' do
          expect(context.errors).to_not be_blank
        end
      end
    end
  end
end
