# frozen_string_literal: true

require 'rails_helper'

module Saasy
  RSpec.describe Saasy::Users::DeleteUser do
    describe '.call' do
      context 'when given valid attributes' do
        let!(:user) { FactoryBot.create(:saasy_user) }

        subject(:context) { Saasy::Users::DeleteUser.call(user: user) }

        it 'succeeds' do
          expect(context).to be_a_success
        end

        it 'updates the deleted at column' do
          expect(context.user.deleted_at).to_not be_nil
        end

        it 'provides a message' do
          expect(context.message).to eq('user.delete.success')
        end
      end
    end
  end
end
