# frozen_string_literal: true

module Saasy
  # Preview all emails at http://localhost:3000/rails/mailers/invitation_mailer
  class InvitationMailerPreview < ActionMailer::Preview
  end
end
