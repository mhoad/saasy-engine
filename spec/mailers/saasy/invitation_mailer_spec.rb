# frozen_string_literal: true

require 'rails_helper'

module Saasy
  RSpec.describe Saasy::InvitationMailer, type: :mailer do
    describe 'invite' do
      let(:invitation) { FactoryBot.create(:saasy_invitation) }

      before do
        @mail = InvitationMailer.invite(invitation)
      end

      context 'headers' do
        it 'renders the subject' do
          expect(@mail.subject).to eq(
            "Invitation to join #{invitation.account.company_name} on #{Saasy.application_name}"
          )
        end

        it 'sends to the right email address' do
          expect(@mail.to).to eq [invitation.email]
        end

        it 'sends from the right email address' do
          expect(@mail.from).to eq ['from@example.com']
        end

        it 'includes the correct url with the users token' do
          expect(@mail.body.encoded).to include invitation.token
        end
      end
    end
  end
end
