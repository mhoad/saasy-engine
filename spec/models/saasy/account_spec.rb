# frozen_string_literal: true

# == Schema Information
#
# Table name: saasy_accounts
#
#  id           :integer          not null, primary key
#  company_name :string(50)       not null
#  subdomain    :string(50)       not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  deleted_at   :datetime
#

require 'rails_helper'

module Saasy
  RSpec.describe Saasy::Account, type: :model do
    it 'has a valid factory' do
      expect(build(:saasy_account)).to be_valid
    end

    # Lazily loaded to ensure it's only used when it's needed
    # RSpec tip: Try to avoid @instance_variables if possible. They're slow.
    let(:account) { build(:saasy_account) }

    describe 'ActiveModel validations' do
      it { expect(account).to validate_presence_of(:company_name) }
      it { expect(account).to validate_length_of(:company_name).is_at_least(2).is_at_most(50) }

      it { expect(account).to validate_presence_of(:subdomain) }
      it { expect(account).to validate_length_of(:subdomain).is_at_least(2).is_at_most(50) }
      it 'should not accept invalid subdomains' do
        invalid_subdomains = %w[admin www 1234 hello1 whats-up]
        invalid_subdomains.each do |subdomain|
          account.subdomain = subdomain
          expect(account).to_not be_valid
        end
      end
    end

    describe 'description' do
      describe 'Database columns' do
        it { expect(account).to have_db_column(:company_name).of_type(:string).with_options(null: false) }
        it { expect(account).to have_db_column(:subdomain).of_type(:string).with_options(null: false) }
        it { expect(account).to have_db_index(:subdomain).unique(true) }
      end
    end
  end
end
