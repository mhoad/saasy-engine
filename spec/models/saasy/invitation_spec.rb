# frozen_string_literal: true

# == Schema Information
#
# Table name: saasy_invitations
#
#  id         :integer          not null, primary key
#  email      :string           not null
#  token      :string           not null
#  account_id :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

module Saasy
  RSpec.describe Saasy::Invitation, type: :model do
    it 'has a valid factory' do
      expect(build(:saasy_invitation)).to be_valid
    end

    # Lazily loaded to ensure it's only used when it's needed
    # RSpec tip: Try to avoid @instance_variables if possible. They're slow.
    let(:invitation) { build(:saasy_invitation) }

    describe 'ActiveModel validations' do
      it { expect(invitation).to validate_presence_of(:email) }
      it { expect(invitation).to validate_presence_of(:account_id) }
    end

    describe 'Associations' do
      it { expect(invitation).to belong_to(:account) }
    end

    describe 'Database columns' do
      it { expect(invitation).to have_db_column(:email).of_type(:string).with_options(null: false) }
      it { expect(invitation).to have_db_column(:account_id).of_type(:integer).with_options(null: false) }
      it { expect(invitation).to have_db_index(:token) }
    end

    describe 'Functionality' do
      it 'generates a unique token' do
        invitation.save
        expect(invitation.token).to be_present
      end
    end
  end
end
