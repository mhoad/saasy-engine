# frozen_string_literal: true

# == Schema Information
#
# Table name: saasy_memberships
#
#  id         :integer          not null, primary key
#  account_id :integer          not null
#  user_id    :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

module Saasy
  RSpec.describe Saasy::Membership, type: :model do
    it 'has a valid factory' do
      expect(build(:saasy_membership)).to be_valid
    end

    # Lazily loaded to ensure it's only used when it's needed
    # RSpec tip: Try to avoid @instance_variables if possible. They're slow.
    let(:membership) { build(:saasy_membership) }

    describe 'ActiveModel validations' do
      it { expect(membership).to validate_presence_of(:account_id) }
      it { expect(membership).to validate_presence_of(:user_id) }
    end

    describe 'Associations' do
      it { expect(membership).to belong_to(:account) }
      it { expect(membership).to belong_to(:user) }
    end

    describe 'Database columns' do
      it { expect(membership).to have_db_index(:account_id) }
      it { expect(membership).to have_db_index(:user_id) }
    end
  end
end
