# frozen_string_literal: true

describe Saasy::InvitationPolicy do
  subject { described_class }

  let(:account) { FactoryBot.create(:saasy_account) }
  let(:admin_user) { FactoryBot.create(:saasy_user) }
  let(:regular_user) { FactoryBot.create(:saasy_user) }

  before do
    admin_user.add_role :admin, account
    account.users << admin_user << regular_user
  end

  permissions :create?, :new? do
    it 'grants access if user is an admin' do
      expect(subject).to permit(admin_user, account.invitations.new)
      expect(subject).to permit(admin_user, account.invitations.create)
    end

    it 'denies access to regular users' do
      expect(subject).to_not permit(regular_user, account.invitations.new)
      expect(subject).to_not permit(regular_user, account.invitations.create)
    end
  end
end
