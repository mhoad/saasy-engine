# frozen_string_literal: true

require 'capybara/rails'
require 'capybara/rspec'

# Setup Capybara to use the much faster rack_test by default
# and whenever you need JS support to use Chrome Headless instead
RSpec.configure do |config|
  config.before(:each, type: :system) do
    driven_by :rack_test
  end

  config.before(:each, type: :system, js: true) do
    driven_by :selenium_chrome_headless
  end
end
