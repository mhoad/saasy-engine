# frozen_string_literal: true

module SubdomainHelpers
  def use_subdomain(subdomain)
    site = "http://#{subdomain}.lvh.me"

    Capybara.configure do |config|
      config.app_host = site
      config.always_include_port = true
    end

    default_url_options[:host] = site.to_s
  end

  def use_default_host
    Capybara.configure do |config|
      config.app_host = 'http://lvh.me'
      config.always_include_port = true
    end

    default_url_options[:host] = 'lvh.me'
    default_url_options[:port] = 3000

    return unless Capybara.current_session.server
    default_url_options[:port] = Capybara.current_session.server.port
  end
end

RSpec.configure do |config|
  config.include SubdomainHelpers, type: :system
  config.before type: :system do
    use_default_host
  end
end
