# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Inviting users', type: :system do
  let(:membership) { FactoryBot.create(:saasy_membership) }

  context 'as an admin' do
    before do
      membership.user.add_role :admin, membership.account
      use_subdomain(membership.account.subdomain)
      login_as(membership.user)
      visit saasy.root_url
    end

    scenario 'invites a user successfully' do
      click_link 'Users'
      click_link 'Invite User'
      fill_in 'Email', with: 'test@example.com'
      click_button 'Invite User'
      expect(page).to have_content('test@example.com has been invited.')
      expect(page.current_url).to eq(saasy.root_url)
    end
  end
end
