
# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Things', type: :system do
  let!(:account_a) { FactoryBot.create(:saasy_membership) }
  let!(:account_b) { FactoryBot.create(:saasy_membership) }

  before do
    # byebug
    FactoryBot.create(:thing, name: 'Account A Thing', account: account_a.account)
    FactoryBot.create(:thing, name: 'Account B Thing', account: account_b.account)
  end

  context 'Account A' do
    before do
      use_subdomain(account_a.account.subdomain)
      login_as(account_a.user)
    end

    scenario 'displays only the thing associated with Account A' do
      # visit saasy.root_url
      # byebug
      visit '/things'
      expect(page).to have_content 'Account A Thing'
      expect(page).to_not have_content 'Account B Thing'
    end
  end

  context 'Account B' do
    before do
      use_subdomain(account_b.account.subdomain)
      login_as(account_b.user)
    end

    scenario 'displays only the thing associated with Account B' do
      # visit saasy.root_url
      visit '/things'
      expect(page).to have_content 'Account B Thing'
      expect(page).to_not have_content 'Account A Thing'
    end
  end
end
