# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Users', type: :system do
  let(:account) { FactoryBot.create(:saasy_account) }
  let(:account_admin) { FactoryBot.create(:saasy_user) }
  let(:user) { FactoryBot.create(:saasy_user) }

  before do
    use_subdomain(account.subdomain)
    account.users << user << account_admin
    account_admin.add_role :admin, account
  end

  context 'as an account admin' do
    before { login_as(account_admin) }

    scenario 'it can remove a user' do
      visit saasy.root_url
      click_link 'Users'
      within('#users') do
        expect(page).to have_content(account_admin.email)
        expect(page).to have_content(user.email)
      end

      within(".user_#{user.id}") do
        click_link 'Remove'
      end

      within('.alert') do
        expect(page).to have_content("#{user.email} has been removed from this account.")
      end

      # Since the admin is the only person left on the account
      # they shouldn't be able to remove themselves
      within('#users') do
        expect(page).to_not have_content 'Remove'
      end

      expect(account.reload.users).to_not include(user)
    end
  end

  context 'as an account user' do
    before do
      login_as(user)
      visit saasy.root_url
      click_link 'Users'
    end

    scenario 'cannot remove other accounts' do
      within(".user_#{account_admin.id}") do
        expect(page).to_not have_content('Remove')
      end
    end

    scenario 'can remove themselves from the account' do
      within(".user_#{user.id}") do
        click_link 'Remove'
      end

      expect(account.reload.users).to_not include(user)
    end
  end
end
