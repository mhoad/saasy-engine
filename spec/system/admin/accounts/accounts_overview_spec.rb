# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Viewing Accounts in Admin Backend', type: :system do
  let(:admin_user) { FactoryBot.create(:saasy_user) }
  let!(:account) { FactoryBot.create(:saasy_account) }

  context 'as a logged in admin user' do
    before do
      admin_user.add_role :admin
      sign_in admin_user
    end

    scenario 'I can view a list of all accounts' do
      visit saasy.admin_accounts_path
      expect(page).to have_content account.company_name
    end
  end
end
