# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Create an Account in Admin Backend', type: :system do
  let(:admin_user) { FactoryBot.create(:saasy_user) }

  context 'as a logged in admin user' do
    before do
      admin_user.add_role :admin
      sign_in admin_user
      visit saasy.admin_accounts_path
    end

    context 'with valid information' do
      scenario 'I can create a new account' do
        click_link 'Create Account'
        fill_in 'Company name', with: 'Test Company'
        fill_in 'Subdomain', with: 'test'
        click_button 'Create Account'

        expect(page).to have_content I18n.t('account.create.success')
        expect(page).to have_content 'Test Company'
        expect(page.current_path).to eq saasy.admin_account_path(Saasy::Account.last)
        expect(Saasy::Account.count).to eq 1
      end
    end

    context 'with invalid information' do
      scenario 'I can not create an account' do
        click_link 'Create Account'
        click_button 'Create Account'

        expect(page).to have_content I18n.t('account.create.failure')
        expect(Saasy::Account.count).to eq 0
      end
    end
  end
end
