# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Edit an Account in Admin Backend', type: :system do
  let(:admin_user) { FactoryBot.create(:saasy_user) }
  let!(:account) { FactoryBot.create(:saasy_account) }

  context 'as a logged in admin user' do
    before do
      admin_user.add_role :admin
      sign_in admin_user
      visit saasy.admin_accounts_path
    end

    scenario 'I can delete an account' do
      click_link 'Delete'

      expect(page).to have_content(I18n.t('account.delete.success'))
      expect(Saasy::Account.count).to eq 0
    end
  end
end
