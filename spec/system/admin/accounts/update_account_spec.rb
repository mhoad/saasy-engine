# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Edit an Account in Admin Backend', type: :system do
  let(:admin_user) { FactoryBot.create(:saasy_user) }
  let!(:account) { FactoryBot.create(:saasy_account) }

  context 'as a logged in admin user' do
    before do
      admin_user.add_role :admin
      sign_in admin_user
      visit saasy.admin_accounts_path
    end

    context 'with valid account attributes' do
      scenario 'I can edit an account' do
        click_link 'Edit'
        fill_in 'Company name', with: 'New Name'
        click_button 'Update Account'

        expect(page).to have_content(I18n.t('account.update.success'))
        expect(Saasy::Account.first.company_name).to eq 'New Name'
        expect(page).to have_content 'New Name'
      end
    end

    context 'with invalid account attributes' do
      scenario 'I can not edit an account' do
        click_link 'Edit'
        fill_in 'Company name', with: ''
        click_button 'Update Account'

        expect(page).to have_content(I18n.t('account.update.failure'))
        expect(Saasy::Account.first.company_name).to eq account.company_name
      end
    end
  end
end
