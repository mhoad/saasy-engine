# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Viewing Accounts in Admin Backend', type: :system do
  let(:admin_user) { FactoryBot.create(:saasy_user) }
  let(:account_admin_user) { FactoryBot.create(:saasy_user) }
  let(:account) { FactoryBot.create(:saasy_account) }

  context 'as a logged in admin user' do
    before do
      admin_user.add_role :admin
      account_admin_user.add_role :admin, account
      admin_user_params = { account: account, user: admin_user }
      account_admin_user_params = { account: account, user: account_admin_user_params }
      Saasy::Memberships::AddUserToAccount.call(admin_user_params)
      Saasy::Memberships::AddUserToAccount.call(account_admin_user_params)
      sign_in admin_user
      visit saasy.admin_accounts_path
      click_link 'View Details'
    end

    scenario 'I can view the details for a given user' do
      expect(page).to have_content account.company_name
    end

    scenario 'I can see the users on the account' do
      expect(page).to have_content admin_user.email
    end

    scenario 'I can see the roles of the users' do
      within('.account-members tbody') do
        expect(page).to have_content 'Admin'
      end
    end
  end
end
