# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Administration sign in', type: :system do
  let(:admin_user) { FactoryBot.create(:saasy_user) }
  let(:regular_user) { FactoryBot.create(:saasy_user) }
  let(:account_admin) { FactoryBot.create(:saasy_user) }
  let(:account) { FactoryBot.create(:saasy_account) }

  before do
    admin_user.add_role :admin
    account_admin.add_role :admin, account
  end

  context 'As an administrator' do
    scenario 'I can view the backend' do
      login_as(admin_user)
      visit saasy.admin_root_path

      expect(page).to have_content('Admin Dashboard')
      expect(page.current_url).to eq('http://lvh.me/admin')
    end
  end

  context 'As an account administrator' do
    scenario 'I can not view the backend' do
      login_as(account_admin)
      visit saasy.admin_root_path

      expect(page).to have_content('You do not have permission to access this page.')
      expect(page.current_url).to eq('http://lvh.me/')
    end
  end

  context 'As a regular user' do
    scenario 'I can not view the backend' do
      login_as(regular_user)
      visit saasy.admin_root_path

      expect(page).to have_content('You do not have permission to access this page.')
      expect(page.current_url).to eq('http://lvh.me/')
    end
  end

  context 'As an unauthenticated user' do
    scenario 'I can not view the backend' do
      visit saasy.admin_root_path

      expect(page).to have_content('You need to sign in or sign up before continuing.')
      expect(page.current_url).to eq('http://lvh.me/users/sign_in')
    end
  end
end
