# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Delete an Account in Admin Backend', type: :system do
  let(:admin_user) { FactoryBot.create(:saasy_user) }
  let!(:regular_user) { FactoryBot.create(:saasy_user) }

  context 'as a logged in admin user' do
    before do
      admin_user.add_role :admin
      sign_in admin_user
      visit saasy.admin_users_path
    end

    scenario 'I can delete an account' do
      expect(Saasy::User.count).to eq 2

      click_link 'Delete'

      expect(page).to have_content I18n.t('user.delete.success')
      expect(Saasy::User.count).to eq 1
    end
  end
end
