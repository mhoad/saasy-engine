# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Viewing Users in Admin Backend', type: :system do
  let(:admin_user) { FactoryBot.create(:saasy_user) }
  let(:account) { FactoryBot.create(:saasy_account) }

  context 'as a logged in admin user' do
    before do
      admin_user.add_role :admin
      params = { account: account, user: admin_user }
      Saasy::Memberships::AddUserToAccount.new(params).call
      sign_in admin_user
    end

    scenario 'I can view the details for a given user' do
      visit saasy.admin_users_path
      click_link 'View Details'

      expect(page).to have_content admin_user.email
      expect(page).to have_content account.company_name
    end
  end
end
