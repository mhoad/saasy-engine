# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Viewing Users in Admin Backend', type: :system do
  let(:admin_user) { FactoryBot.create(:saasy_user) }
  let!(:regular_user) { FactoryBot.create(:saasy_user) }

  context 'as a logged in admin user' do
    before do
      admin_user.add_role :admin
      sign_in admin_user
    end

    scenario 'I can view a list of all users' do
      visit saasy.admin_users_path
      expect(page).to have_content admin_user.email
      expect(page).to have_content regular_user.email
    end
  end

  context 'as a regular user' do
    before do
      sign_in regular_user
    end

    scenario 'I can not access the page' do
      visit saasy.admin_users_path
      expect(page).to have_content('You do not have permission to access this page.')
      expect(page.current_url).to eq('http://lvh.me/')
    end
  end
end
