# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Signups', type: :system do
  before do
    visit saasy.root_path
    click_link 'Create an account'
  end

  context 'with valid information' do
    scenario 'creating an account' do
      fill_in 'Company name', with: 'test'
      fill_in 'Subdomain', with: 'test'
      fill_in 'Email', with: 'test@example.com'
      fill_in 'Password', with: 'password'
      fill_in 'Password confirmation', with: 'password'
      click_button 'Create Account'

      expect(page).to have_content('Signed in as test@example.com')
      expect(page.current_url).to eq('http://test.lvh.me/')

      within('.alert') do
        expect(page).to have_content(I18n.t('account.create.success'))
      end

      expect(Saasy::Account.count).to eq 1
      expect(Saasy::User.count).to eq 1
      expect(Saasy::Membership.count).to eq 1
      # Ensure the user has admin role for the account but not a global admin
      expect(Saasy::User.first.has_role?(:admin, Saasy::Account.first)).to eq true
      expect(Saasy::User.first.has_role?(:admin)).to eq false
    end
  end

  context 'with invalid information' do
    scenario 'creating an account' do
      click_button 'Create Account'

      within('.alert') do
        expect(page).to have_content(I18n.t('account.create.failure'))
      end

      expect(page).to have_content("Company name can't be blank")
      expect(Saasy::Account.count).to eq 0
      expect(Saasy::User.count).to eq 0
      expect(Saasy::Membership.count).to eq 0
    end
  end
end
