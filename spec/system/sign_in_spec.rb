# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'User account sign in', type: :system do
  let!(:account) { FactoryBot.create(:saasy_account) }
  let!(:user) { FactoryBot.create(:saasy_user) }
  let(:non_member_user) { FactoryBot.create(:saasy_user) }

  before do
    params = { account: account, user: user }
    Saasy::Memberships::AddUserToAccount.call(params)
    user.add_role :admin, account
    use_subdomain(account.subdomain)
  end

  scenario 'signs in as an account owner successfully' do
    visit saasy.new_user_session_url
    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Log in'

    expect(page).to have_content("Signed in as #{user.email}")
    expect(page.current_url).to eq(saasy.root_url(subdomain: account.subdomain))
  end

  scenario 'sends unauthenticated users to a sign in page' do
    visit saasy.root_url
    expect(page.current_url).to eq(saasy.new_user_session_url)
  end

  scenario 'attempts sign in with invalid password and fails' do
    visit saasy.new_user_session_url
    fill_in 'Email', with: user.email
    fill_in 'Password', with: 'incorrect_password'
    click_button 'Log in'

    expect(page).to_not have_content("Signed in as #{user.email}")
    expect(page).to have_content('Invalid Email or password.')
    expect(page.current_url).to eq(saasy.new_user_session_url)
  end

  scenario 'does not allow users who are not members of the account to sign in' do
    visit saasy.new_user_session_url
    fill_in 'Email', with: non_member_user.email
    fill_in 'Password', with: non_member_user.password
    click_button 'Log in'

    expect(page).to have_content('You are not allowed to view this account.')
    expect(page.current_url).to_not eq(saasy.root_url(subdomain: account.subdomain))
  end
end
